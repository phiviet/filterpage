import {
    UserService,
    AuthService,
    ShareholderMeetingSerivce
} from ".";
import {
    IRepositories,
    IServices,
    IUserService,
    IAuthService,
    IContrib,
    IShareholderMeetingService
} from "../interfaces";
import Config from "../config";

export default class Services implements IServices {
    public userService: IUserService;
    public authService: IAuthService;

    public shareholderMeetingService: IShareholderMeetingService;

    /**
     * Create new services DI
     */
    constructor(contructor: {repositories: IRepositories, contrib: IContrib, config: Config }) {
        this.userService = new UserService(contructor.repositories);
        this.authService = new AuthService(contructor.repositories, contructor.contrib, contructor.config.TokenConfig);

        this.shareholderMeetingService = new ShareholderMeetingSerivce(contructor.repositories);
    }
}
