import { IRepositories, IShareholderMeetingService, IShareholderMeetingRepository, IShareholderMeeting, IPage } from "../interfaces";
import { Error } from "../constants";

export class ShareholderMeetingSerivce implements IShareholderMeetingService {

    // createShareholderMeeting(body: IShareholderMeeting): Promise<any> {
    //     try {
            
    //     }
    // }
    private shareholderMeetingRepo : IShareholderMeetingRepository;

    constructor(repoList: IRepositories) {
        this.shareholderMeetingRepo = repoList.shareholderMeetingRepository;
    }
    async getAllShareholderMeeting(): Promise<any> {
        try {
            const meeting = await this.shareholderMeetingRepo.findAll()
            if (!meeting) {
                throw Error.NOT_FOUND;
            }
            return meeting;
        } catch (err) {
            throw err;
        }
    }

    async findShareholderMeeting(body: IPage): Promise<any> {
        const meetings = await this.shareholderMeetingRepo.find(body);
        return meetings;
    }

}