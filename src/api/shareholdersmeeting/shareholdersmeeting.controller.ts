import { IRouteConfig, IShareholderMeetingService, CError } from "../../interfaces";
import { Request, Response, NextFunction } from "express";

export class ShareholderMeetingController {

    private shareholderMeetingService: IShareholderMeetingService;
    constructor(config: IRouteConfig) {
        this.shareholderMeetingService = config.services.shareholderMeetingService;
    }

    public async getShareHolderMeeting(req: Request, res: Response, next: NextFunction): Promise<void> {
        try {
            // const shareholderMeetings = await this.shareholderMeetingService.getAllShareholderMeeting();
            const shareholderMeetings = await this.shareholderMeetingService.getAllShareholderMeeting();
            res.json({ shareholderMeetings });

        } catch (err) {
            next(new CError(err));
        }
    }

    public async findShareHolderMeeting(req: Request, res: Response, next: NextFunction): Promise<void> {
        try {
            const body = req.body;
            // const shareholderMeetings = await this.shareholderMeetingService.getAllShareholderMeeting();
            const shareholderMeetings = await this.shareholderMeetingService.findShareholderMeeting(body);
            res.json({ shareholderMeetings });

        } catch (err) {
            next(new CError(err));
        }
    }

    // public async createShareHolderMeeting(req: Request, res: Response, next: NextFunction): Promise<void> {
    //     try {
    //         const meeting = req.body;
    //         const meetingcreated = await this.shareholderMeetingService.getAllShareholderMeeting();
    //     }
    //     catch (err) {
    //         next(new CError(err));
    //     }
    // }
}