import { IRouteConfig } from "../../interfaces";
import { Router } from "express";
import { ShareholderMeetingController } from ".";

export const ShareholderMeetingRoutes = (config: IRouteConfig): Router => {
    const router = Router();
    const controller =  new ShareholderMeetingController(config);

    const shareholderMeeting = new ShareholderMeetingController(config);
    router.get('/getAll',controller.getShareHolderMeeting.bind(controller));
    router.post('/find',controller.findShareHolderMeeting.bind(controller));
    // router.post('/create',controller.getShareHolderMeeting.bind(controller));
    return router;
}