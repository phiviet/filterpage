import { IUserRepository } from ".";
import { IShareholderMeetingRepository } from ".";

export interface IRepositories {
    userRepository: IUserRepository;

    shareholderMeetingRepository: IShareholderMeetingRepository;
}
