import { IShareholderMeeting, IPage } from "..";

export interface IShareholderMeetingRepository {
    findAll():Promise<IShareholderMeeting[]>;
    find(body: IPage):Promise<IShareholderMeeting[]>;
    // create(body: IShareholderMeeting):Promise<any>;
}