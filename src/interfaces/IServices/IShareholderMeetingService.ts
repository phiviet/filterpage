import { IShareholderMeeting, IPage } from "..";

export interface IShareholderMeetingService {

    getAllShareholderMeeting():Promise<any>;
    // createShareholderMeeting(body: IShareholderMeeting):Promise<any>;
    findShareholderMeeting(body: IPage):Promise<IShareholderMeeting[]>;
}
