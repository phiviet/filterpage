export * from "./IAuthService";
export * from "./IUserService";

export * from "./IShareholderMeetingService";

export * from "./IServices";
