import { IAuthService, IUserService, IShareholderMeetingService } from ".";

export interface IServices {
    authService: IAuthService;
    userService: IUserService;
    
    shareholderMeetingService: IShareholderMeetingService;
}
