import * as mongoose from 'mongoose';

export interface IShareholderMeeting {
    id?: string;
    name: string;
    time: Date;
}

export interface IShareholderMeetingModel extends IShareholderMeeting, mongoose.Document {
    id: string;
}

export interface IPage{
    element: number;
    page: number;
}