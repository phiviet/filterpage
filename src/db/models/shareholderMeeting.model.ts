import * as mongoose from 'mongoose';
import { Schema, model } from "mongoose";
import { IShareholderMeeting, IShareholderMeetingModel } from "../../interfaces";
import { CollectionNames } from '../../constants';

const shareholderMeetingSchema = new Schema(
    {
        name: {
            type: String,
            required: true,
        },
        time: {
            type:Date
        }
    }
);

export const shareholderMeetingModel = model<IShareholderMeetingModel>(CollectionNames.ShareholderMeeting,shareholderMeetingSchema);