import { IShareholderMeetingRepository, IShareholderMeeting, IPage } from "../../interfaces";
import { shareholderMeetingModel } from "../models/shareholderMeeting.model";

export class ShareholderMeetingRepository implements IShareholderMeetingRepository {

    async find(body: IPage): Promise<any> {
        const element = body.element;
        const page = body.page;
        if(!body.element){

        }
        
        const meetings = await shareholderMeetingModel
            .find()
            .limit(element)
            .skip(element*page - element);
        return meetings as IShareholderMeeting[];
    }
    async findAll(): Promise<any> {
        const shareholderMeeting = await shareholderMeetingModel.find({});
        return shareholderMeeting as IShareholderMeeting[];
    }

    // async create(body: IShareholderMeeting): Promise<any> {
    //     // const meetingCreated = await shareholderMeetingModel.update({})
    // }
}
