import {
    IRepositories,
    IUserRepository,
    IShareholderMeetingRepository
} from "../../interfaces";
import { UserRepository } from ".";
import { ShareholderMeetingRepository } from "./shareholderMeeting.repository";

export default class Repositories implements IRepositories {
    public userRepository: IUserRepository;

    public shareholderMeetingRepository: IShareholderMeetingRepository
    /**
     * Create new Repository DI
     */
    constructor() {
        this.userRepository = new UserRepository();

        this.shareholderMeetingRepository = new ShareholderMeetingRepository();
    }
}
